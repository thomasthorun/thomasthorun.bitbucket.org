/* creates empty location object with 'loading' effect */
function InitializeLocation(locations, name) {
    this.name = ko.observable(name);
    this.ip = ko.observable("");
    this.country = ko.observable("");
    this.region = ko.observable("");
    this.city = ko.observable("");
    this.timezone = ko.observable("");
    this.latitude = ko.observable("");
    this.longitude = ko.observable("");
    this.marker = "";
    locations.push(this);
    if(name == "my location") $('input.site:last').removeClass().addClass("my");
    LoaderAnimation();
}

/* get data object with ip-api json template */
function MyLocation(data) {
    this.name = ko.observable(data.name);
    this.ip = ko.observable(data.query);
    this.country = ko.observable(data.country);
    this.region = ko.observable(data.region);
    this.city = ko.observable(data.city);
    this.timezone = ko.observable(data.timezone);
    this.latitude = ko.observable(data.lat);
    this.longitude = ko.observable(data.lon);
    this.marker = InsertMarker(data.lat, data.lon, this.name());
}

/* get data object with freegeoip json template */
function SiteLocation(data) {
    this.name = ko.observable(data.name);
    this.ip = ko.observable(data.ip);
    this.country = ko.observable(data.country_name);
    this.region = ko.observable(data.region_name);
    this.city = ko.observable(data.city);
    this.timezone = ko.observable(data.time_zone);
    this.latitude = ko.observable(data.latitude);
    this.longitude = ko.observable(data.longitude);
    this.marker = InsertMarker(data.latitude, data.longitude, this.name());
}

/* replace empty location object with a filled one using 'splice' */
function UpdateLocation(locations, init, location, locationClass) {
    location.name = init.name();
    var index = locations().indexOf(init);
    var loc;
    if (index != -1){
        if(locationClass =='site') loc = new SiteLocation(location);
        if (locationClass =='my') loc = new MyLocation(location);
        locations.splice(index, 1, loc);
        index = locations().indexOf(loc) + 1;    
        CreateTooltip(loc, index);
            // change title color to blue if this is my location
        if (locationClass =='my') {
            $('#locs >:nth-child(' + index +')').children().children(".site")
                .removeClass().addClass(locationClass);
        }
        // remove loader object from the info button
        $('#locs > :nth-child(' + index + ')').children().children(".has-tip")
            .children(".info").children(".load").remove();
        // add info icon to the info button
        $('#locs > :nth-child(' + index + ')').children().children(".has-tip")
            .children(".info").append("<i class='fi-info'></i>");
    }
}

/* validation for site URL */
function WrongUrl(name) {
    $(document).foundation('reflow'); //update foundation to show tooltip correctly
    if(name.length < 3 || name.indexOf(".") == -1 || name.indexOf(" ") != -1 ) {
        // put dropdown showing invalid URL.
        $('#sbtn').wrap("<a id='drop' data-dropdown='drop2' aria-controls='drop2' aria-expanded='false'></a>");
        Foundation.libs.dropdown.toggle($('#drop'));
        // onmouseleave close dropdown
        $('#sbtn').on('mouseleave', function() {
            Foundation.libs.dropdown.close($('#drop2'));
        });
        // remove dropdown
        $('#drop').contents().unwrap();
        return true;
    }
    else {
        Foundation.libs.dropdown.close($('#drop2'));
        $(document).foundation('reflow');
        return false;
    }
}

/* Create tooltip with location info */
function CreateTooltip(location, index) {
    $('#locs > :nth-child(' + index + ')').children().children(".has-tip").attr({title:"Ip: " + location.ip() 
        + "<br>Country: " + location.country() + "<br>Region: " + location.region() + "<br>City: " 
        + location.city() + "<br>Timezone: " + location.timezone() + "<br>Latitude: " 
        + location.latitude() + "<br>Longitude: " + location.longitude()
    });
    $(document).foundation('reflow'); //update foundation to show tooltip correctly
}

/* Change button icon to indicate error, change colors of buttons to red */
function UpdateLocationWithError(locations, init, titleClass, status){
    index = locations.indexOf(init);
    if (index !== -1){
        index += 1;
        // remove loader object from the info button
        $('#locs > :nth-child(' + index + ')').children().children(".has-tip").children(".info")
            .children(".load").remove();
        // add error icon to the info button
        $('#locs > :nth-child(' + index + ')').children().children(".has-tip").children(".info")
            .removeClass().addClass("err").append("<i class='fi-x-circle'></i>");
        // change location object title to red
        $('#locs > :nth-child(' + index + ')').children().children(titleClass)
            .removeClass().addClass("titleerr");
        // change trash icon to red
        $('#locs > :nth-child(' + index + ')').children().children(".trash")
            .removeClass().addClass("trasherr");
        CreateErrorTooltip(index, status);
    }
}

/* Create tooltip with error info */
function CreateErrorTooltip(index, status) {
    // status '404' = bad request to freegeoip, probably wrong URL
    if(status === 404) {
        $('#locs > :nth-child(' + index + ')').children().children(".has-tip")
            .attr({title:"Error to load this request! <br> Maybe due to a wrong URL"});
    }
    // status '0' = probably popup blocker
    else {
        $('#locs > :nth-child(' + index + ')').children().children(".has-tip")
            .attr({title:"Error to load this request! <br> Maybe due to some popup<br> or advertisement blocker"});   
    }
    $(document).foundation('reflow'); //update foundation to show tooltip correctly
}