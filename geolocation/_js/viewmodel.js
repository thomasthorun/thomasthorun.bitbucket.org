function ViewModel() {
    var self = this;

    /* Data */
    self.locations = ko.observableArray([]);
    self.siteUrl = ko.observable(""); // gets input site value
    self.myLocationExists = ko.observable(false); // aux for enable/disable my location button

    //determine when activate/deactivate input my location button
    self.enableMyLocationButton = ko.computed(function() {
        if (self.locations().length >= 6 || self.myLocationExists()) return false;
        else return true;
    });

    /* Operations */
    /* gets site data from freegeoip JSON, convert into location instances, then populate self.locations */
    self.getSiteLocation = function() {
        if (WrongUrl(self.siteUrl())) return;
        var init = new InitializeLocation(self.locations, self.siteUrl().toLowerCase());
        self.siteUrl("");
        //request json from freegeoip
        $.getJSON('http://freegeoip.net/json/' + init.name(), function(location) {
            UpdateLocation(self.locations, init, location, 'site');
        }).fail( function(jqXHR) {
            UpdateLocationWithError(self.locations, init, '.site', jqXHR.status);
        });
    };
    
    /* load my location data from ip-api, convert it to location instances, then populate self.locations */
    self.getMyLocation = function() {
        var init = new InitializeLocation(self.locations, "my location");
        self.myLocationExists(true);
        //request json from ip-api
        $.getJSON('http://ip-api.com/json/', function(location) {
            UpdateLocation(self.locations, init, location, 'my');   
        }).fail( function(jqXHR) {
            UpdateLocationWithError(self.locations, init, '.my', jqXHR.status);
        });
    };

    /* onclick 'delete' - remove location from the list */
    self.removeLocation = function(location) {
        self.locations.remove(location);
        //RemoveMarker(location.marker);
        if (location.name() === "my location") self.myLocationExists(false);
    };
}

ko.applyBindings(new ViewModel());