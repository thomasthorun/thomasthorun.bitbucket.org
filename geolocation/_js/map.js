var map;

function InitializeMap() {
    var mapOptions = {
        zoom: 2
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    map.setCenter(new google.maps.LatLng(0, 0));
}

/* Create marker with site name label or my location label */
function InsertMarker(lat, lon, title) {
    if (title == "my location") var labelsClass = "myLocationLabel";
    else var labelsClass = "siteLabel";
    return new MarkerWithLabel ( {
        map: map,
        position: new google.maps.LatLng(lat, lon),
        title: title,
        labelContent: title,
        labelAnchor: new google.maps.Point(title.length*5/2 + 4, -2), //position label below marker, centralized.
        labelClass: labelsClass
    }); 
}

function RemoveMarker(marker) {
    marker.setMap(null);    
}

google.maps.event.addDomListener(window, 'load', InitializeMap);
